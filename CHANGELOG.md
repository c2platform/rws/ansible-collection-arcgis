# CHANGELOG

## 1.0.22 (  )

* [`fme_flow`](./roles/fme_flow/) added tag `win_service`.

## 1.0.21 ( 2024-12-19 )

* `c2platform.wincore` → `1.0.15`.

## 1.0.20 ( 2024-11-14 )

* [`tomcat`](./roles/tomcat/) added tags `install`, `application`, `tomcat` and
  changed `include_*` to `import_*`.
* [`java`](./roles/java/) added tags `install`, `application`, `java`.
* [`fme_flow`](./roles/fme_flow/) added tags `install`, `application`,
  `fme_flow` and changed `include_*` to `import_*`.
* [`vertigis_studio`](./roles/vertigis_studio/) integrated certificates role
  `c2platform.core.cacerts2` with variable
  `vertigis_studio_cacerts2_certificates`
* [`fme_flow`](./roles/fme_flow/) use `c2platform.core.replace_slashes` for
  **Extract to...** task.

## 1.0.19 ( 2024-10-01 )

* Collection deprecated.
* [`fme_flow`](./roles/fme_flow/) variable `fme_flow_config_enabled` replaced by
  `fme_flow_api_config['enabled']`.
* [`fme_flow`](./roles/fme_flow/) added variable `fme_flow_include_role_tomcat`
  which can be used to include `c2platform.gis.tomcat` role in the `fme_flow`
  role. Removed `fme_flow_install_command_notify`; this variable is now obsolete.
* [`fme_flow`](./roles/fme_flow/) `fme_flow_versions` default an empty `{}` so
  it can support additional downloads.
* [`fme_flow`](./roles/fme_flow/) database tasks using `run_once: true`.
* [`fme_flow`](./roles/fme_flow/) added `fme_flow_win_resource_groups_disabled`.
* [`fme_flow`](./roles/fme_flow/) added handler **Wait for WebSocket / 7078**.
* [`arcgis_webadaptor`](./roles/arcgis_webadaptor) removed
  `arcgis_webadaptor_vdirname`. This variable is no longer used.
* [`arcgis_webadaptor`](./roles/arcgis_webadaptor) Updated adapter configuration
  to be idempotent, preventing unnecessary reconfigurations that could cause
  service unavailability.
* [`arcgis_server`](./roles/arcgis_server) new var `arcgis_server_license_file`
  and improved license server task with exception detection and handling.
* [`arcgis_server`](./roles/arcgis_server),
  [`arcgis_portal`](./roles/arcgis_portal),
  [`arcgis_datastore`](./roles/arcgis_datastore) replaced fact
  `_extracted_folder_name` with new `arcgis_server_download_extract_folder`,
  `arcgis_portal_download_extract_folder`,
  `arcgis_portal_webstyles_extract_folder`,
  `arcgis_portal_geoevent_extract_folder` that also utilizes
  `c2platform.wincore.download_extract_folder` filter.
* [`arcgis_webadaptor`](./roles/arcgis_webadaptor) new "experimental" variable
  `arcgis_webadaptor_dsc_resources` that utilizes
  [`ArcGIS`](https://github.com/Esri/arcgis-powershell-dsc) PowerShell DSC
  Module.
* [`arcgis_webadaptor`](./roles/arcgis_webadaptor) include role
  `c2platform.wincore.win` with variables `arcgis_webadaptor_win_resources` and
  `arcgis_webadaptor_win_resources_types`.

## 1.0.18 ( 2024-08-15 )

* [`fme_flow`](./roles/fme_flow/) api toevoegingen `config.yml`.
* [`fme_flow`](./roles/fme_flow/) clean `config.yml`.

## 1.0.17 ( 2024-07-12 )

* [`fme_flow`](./roles/fme_flow/) api toevoegingen `config.yml`.
* [`fme_flow`](./roles/fme_flow/) clean `config.yml`.

## 1.0.15 ( 2024-06-24 )

*  [`cmk_server`](./roles/cmk_server) `Add
   newly host-alias services`.

## 1.0.14 ( 2024-06-26 )

* `c2platform.wincore` → `1.0.13`.
* [`pg_admin`](./roles/pg_admin/) Role deleted (install now with chocolatey)

## 1.0.13 ( 2024-05-30 )

* [`bridge`](./roles/bridge/) added with parameterize config.

## 1.0.12 ( 2024-05-28 )

* [`vertigis_studio`](./roles/vertigis_studio/) new role.
* [`arcgis_portal`](./roles/arcgis_portal/) added `environment` with `AGSPORTAL`
  to create portal site.
* [`arcgis_portal`](./roles/arcgis_portal/) added error handling to task to
  create portal site using `failed_when`.
* [`fme_flow`](./roles/fme_flow/) added variable `fme_flow_win_resources_types`.
* `.gitlab-ci.yml` based on `c2platform.core`.
* [fme_flow](./roles/fme_flow/) added variable `fme_flow_win_resources_types`.

## 1.0.11 ( 2024-04-26 )

* Upgrade to `c2platform.wincore` `1.0.10`.
* [`fme_flow`](./roles/fme_flow/) using filter `c2platform.core.replace_slashes`
  for `win_package`.
* [`fme_flow`](./roles/fme_flow/) new variables
  `fme_flow_install_command_ignore_errors` and
  `fme_flow_install_command_result_yml`.

## 1.0.10 ( 2024-04-18 )

*  [`cmk_server`](./roles/cmk_server) disable because of checkmk overload `Add
   newly discovered services`.
## 1.0.9 ( 2024-04-12 )

*  [`cmk_agent`] (./roles/cmk_agent)  reviewed / refactored and documented.
*  [`cmk_server`](./roles/cmk_server) added `Add newly discovered services`.

## 1.0.8 ( 2024-04-02 )

*  [`cmk_agent`] (./roles/cmk_agent)  integrated `download` role.
*  [`cmk_server`](./roles/cmk_server) added `cmk_server_delegate_to`.

## 1.0.7 ( 2024-02-19 )

* [`arcgis_server`](./roles/arcgis_server) added role for fme. And added feature
  to stop and disable service.

## 1.0.6 ( 2024-02-15 )

* [`cmk_server`](./roles/cmk_server/) variables renamed: `cmk_site` →
  `cmk_server_site`, `cmk_automation_user` → `cmk_server_automation_user`,
  `cmk_server_automation_secret` → `cmk_server_automation_secret`, `cmk_server_folders` →
  `cmk_server_folders`.
* [`fme_form`](./roles/fme_form/) variables renamed: `fme_installer_path` →
  `fme_flow_installer_path`, `fme_extraction_path` → `fme_form_extraction_path`,
  `fme_installation_args` → `fme_form_installation_args`,
  `fme_installation_path` → `fme_form_installation_path`, `form_version` →
  `fme_form_version`.
* [`fme_flow`](./roles/fme_flow/) variables renamed: `fme_services` →
  `fme_flow_services`, `fme_services_manage` → `fme_flow_services_manage`,
  `fme_database` → `fme_flow_database`.

## 1.0.5 ( 2024-02-14 )

* Various changes

## 1.0.4 ( 2024-01-31 )

* Add missing `README.md` for `bridge` and `cmk_server` role.

## 1.0.3 ( 2024-01-31 )

* [`fme_flow`](./roles/fme_flow/) new variable `fme_flow_win_resources`.
* [`fme_flow`](./roles/fme_flow/) new `fme_flow_install_command_notify` to notify
  resources when installer runs.
* [`java`](./roles/java/) integrated `c2platform.core.cacerts2` to create / manage
  SSL/TLS certificates and Java KeyStores.
* [`fme_flow`](./roles/fme_flow/) integrated `c2platform.core.cacerts2` to create
  / manage SSL/TLS certificates and Java KeyStores.
* [`fme_flow`](./roles/fme_flow/) add `fme_flow_config_enabled`.
* [`tomcat`](./roles/tomcat/) integrated `c2platform.core.cacerts2` to create /
  manage SSL/TLS certificates and Java KeyStores.
* [`fme_flow`](./roles/fme_flow/) new var `fme_flow_folders`.
* [`tomcat`](./roles/tomcat/) new var `tomcat_acl` to manage MS Windows ACLs.
* [`fme_flow`](./roles/fme_flow/) removed debug statement and added var `fme_flow_debug` to
  write commands to file system.
* [`fme_flow`](./roles/fme_flow/) added ability to create FME Flow Database using
  `fme_database`. Default disabled.
* [`fme_flow`](./roles/fme_flow/) added ability to manage FME services with dict
  `fme_services` and boolean `fme_services_manage`. Default disabled.
* [`fme_flow`](./roles/fme_flow/) new role for FME Flow.
* [`tomcat`](./roles/tomcat/) `tomcat_proxy` dictionary for downloads via proxy
  server.
* [`tomcat`](./roles/tomcat/) using download role. Var `tomcat_tmp` →
  `tomcat_temp_dir`.

## 1.0.2 ( 2023-09-08 )

* [`arcgis_webadaptor`](./roles/arcgis_webadaptor) reviewed / refactored and
  documented.
* [`arcgis_portal`](./roles/arcgis_webadaptor) reviewed / refactored and
  documented.
* Removed role `arcgis_webadaptor_configure`. This role is integrated in
  [`arcgis_webadaptor`](./roles/arcgis_webadaptor)

## 1.0.1 ( 2023-06-27 )

* [`arcgis_datastore`](./roles/arcgis_datastore) new role for ArcGIS Datastore.
* [`arcgis_federation`](./roles/arcgis_federation) new role for ArcGIS Federation.
* [`arcgis_portal`](./roles/arcgis_portal) new role for ArcGIS Portal.
* [`arcgis_pro`](./roles/arcgis_pro) new role for ArcGIS Pro.
* [`arcgis_webadaptor_configure`](./roles/arcgis_webadaptor_configure) new role
  for ArcGIS Web Adaptor Configure.
* [`cmk_agent`](./roles/cmk_agent) new role for Checkmk Agent.
* [`cmk_server`](./roles/cmk_server) new role for Checkmk Server.

## 1.0.0 ( 2023-06-16 )

* [`fme_flow`](./roles/fme_flow/) new role for FME Flow.
* [`java`](./roles/java) new role for Java.
* [`tomcat`](./roles/tomcat) new role for Apache Tomcat.

## 0.1.1 ( 2023-06-01 )

* [`arcgis_webadaptor`](./roles/arcgis_webadaptor) new role for ArcGIS Web Adaptor.
* [`arcgis_server`](./roles/arcgis_server) enhanced.

## 0.1.0 ( 2023-05-17 )

* [`arcgis_server`](./roles/arcgis_server) new role for ArcGIS Server.
