# Ansible Role c2platform.gis.java

A simple Ansible Role that installs Java on MS Windows servers. Similar in intend to [c2platform.core.java](https://gitlab.com/c2platform/ansible-collection-core/-/blob/master/roles/java/README.md)

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`java_version`](#java_version)
  - [`java_proxy`](#java_proxy)
  - [`java_cacerts2_certificates`](#java_cacerts2_certificates)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `java_version`

Use dict `java_versions` to define your Java versions eligible for installation, for example:

```yaml
java_install_dir: 'D:\Java\{{ java_version }}'
java_versions:
  9.0.4:
    url: https://download.java.net/java/GA/jdk9/9.0.4/binaries/openjdk-9.0.4_windows-x64_bin.tar.gz
    checksum: 53bcb5d15cd19c6e2ed0fdfed7b932d797377be7
    checksum_algorithm:
    home: '{{ java_install_dir }}\jdk-9.0.4'
```

### `java_proxy`

To download using a proxy server you can use dict `java_proxy` for example:

```yaml
java_proxy:
  url: http://webproxy.whatever.nl:8080
  username: proxy_user
  password: proxy_password
  enabled: true
```

### `java_cacerts2_certificates`

TODO

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
---
- name: fme.yml
  hosts: fme

  roles:
    - {role: c2platform.wincore.win, tags: ["fme", "windows"]}
    - {role: c2platform.gis.java, tags: ["fme", "java"]}
    - {role: c2platform.gis.tomcat, tags: ["fme", "tomcat"]}
    - {role: c2platform.gis.fme, tags: ["fme"]}
```

