# Ansible Role c2platform.gis.fme_form

- [Requirements](#requirements)
- [Role Variables](#role-variables)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->


## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
---
- name: fme.yml
  hosts: fme

  roles:
    - {role: c2platform.wincore.win, tags: ["fme", "windows"]}
    - {role: c2platform.gis.java, tags: ["fme", "java"]}
    - {role: c2platform.gis.tomcat, tags: ["fme", "tomcat"]}
    - {role: c2platform.gis.fme, tags: ["fme"]}
```

See also [ansible-gis/plays/gis/fme.yml](https://gitlab.com/c2platform/rws/ansible-gis/-/blob/320700423b214c65658e0b5faed0484f66bc86e4/plays/gis/fme.yml)
