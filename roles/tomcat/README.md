# Ansible Role c2platform.gis.tomcat

The purpose of this Ansible role is to facilitate the installation and
management of Tomcat on Microsoft Windows hosts. By leveraging this role, users
can automate the execution of the steps outlined in the
[Apache Tomcat User Guide](https://tomcat.apache.org/tomcat-9.0-doc/setup.html).
This role streamlines the deployment process, ensuring a seamless and efficient
setup of Tomcat environments, making it a valuable tool for managing Tomcat
installations on MS Windows hosts.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`tomcat_version` and `tomcat_versions`](#tomcat_version-and-tomcat_versions)
  - [`tomcat_proxy`](#tomcat_proxy)
  - [`tomcat_acl`](#tomcat_acl)
  - [`tomcat_win_resources`](#tomcat_win_resources)
  - [`tomcat_cacerts2_certificates`](#tomcat_cacerts2_certificates)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `tomcat_version` and `tomcat_versions`

Use dictionary `tomcat_versions` the Tomcat versions that can be installed.

```yaml
tomcat_version: 9.0.68
tomcat_versions:
  9.0.68:
    url: https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.68/bin/apache-tomcat-9.0.68.exe
    checksum: ebeb6a8de452bbb7866e8e06ccbed7b3a9fa705b557551ccadde45928b8e7d39
    checksum_algorithm: sha256
```

### `tomcat_proxy`

Dictionary `tomcat_proxy` is passed as `download_proxy` to
[c2platform.wincore.download](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/tree/master/roles/download) role.

### `tomcat_acl`

The `tomcat_acl` dictionary is utilized for managing Microsoft Windows ACLs.
This dictionary streamlines the process by reusing Ansible tasks from the
`win_acl` dictionary within the
[`c2platform.wincore.win`](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/blob/master/roles/win/README.md?ref_type=heads)
role.

By default, it grants full control (`FullControl`) to the `fme_flow_svcaccount`
for the `tomcat_home` path. If this default behavior is not desired, you have
the flexibility to configure it using `when: false`.

```yaml
tomcat_acl:
  name: "ACL service account"
  user: "{{ fme_flow_svcaccount }}"
  path: "{{ tomcat_home }}"
  type: allow
  rights: FullControl
  notify: Restart Tomcat service
  when: true
```
### `tomcat_win_resources`

The `tomcat_win_resources` variable is designed to manage a wide range of
resources through various Ansible modules. For this purpose it integrates the
`c2platform.wincore.win` role. For comprehensive details and guidelines, you can
refer to the documentation
[documentation](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/tree/master/roles/win)
associated with this role.

When applied to this Tomcat role, `tomcat_win_resources` becomes particularly
useful for handling Tomcat XML files, among other functions.

For a deeper understanding and practical insights, the community website offers
two detailed guides:

* [Setting Up FME Flow with Ansible](https://c2platform.org/en/docs/howto/rws/arcgis/fme_flow/)
* [Tomcat SSL/TLS and Java Keystore and TrustStore Configuration for Linux and Windows Hosts](https://c2platform.org/en/docs/howto/rws/certs/)

### `tomcat_cacerts2_certificates`

Use dict `tomcat_cacerts2_certificates` to create / manage SSL/TLS certificates
and / or Java KeyStores. The example configuration belows shows creation and
deployment of a private key, a certificate and a Java KeyStore to the Tomcat
`conf` directory.

```yaml
tomcat_cacerts2_certificates:
  - common_name: "{{ inventory_hostname }}"  # alias
    country_name: NL
    locality_name: Den Haag
    organization_name: C2
    organizational_unit_name: C2 GIS Platform Team
    state_or_province_name: Zuid-Holland
    subject_alt_name:
      - "DNS:{{ inventory_hostname }}"
      - "DNS:{{ inventory_hostname }}.apps.c2platform.org"
    ansible_group: tomcat
    deploy:
      key:
        dir: "{{ tomcat_home }}/conf"
      crt:
        dir: "{{ tomcat_home }}/conf"
      keystore:
        dir: "{{ tomcat_home }}/conf"
        password: secret
        notify: Restart Tomcat service
```

Refer to
[Tomcat SSL/TLS and Java Keystore and TrustStore Configuration for Linux and Windows Hosts | C2 Platform](https://c2platform.org/en/docs/howto/rws/certs/)
for more information.

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }
```