# Ansible Role c2platform.gis.cmk_agent

This role streamlines the process of deploying the [Checkmk agent](https://docs.checkmk.com/latest/en/agent_windows.html), ensuring consistent monitoring capabilities across Windows machines. It downloads the agent installer package and installs it. It starts the Checkmk agent service and sets it to automatically start on system boot.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [cmk\_agent\_win\_get\_url](#cmk_agent_win_get_url)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)


## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### cmk_agent_win_get_url

This role uses [ansible.windows.win_get_url](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_get_url_module.html) to download the Windows Installer ( MSI ). You can use optional var / dict `cmk_agent_win_get_url` to configure a proxy server and / or the credentials to access the download site.

```yaml
cmk_agent_win_get_url:
  proxy_url:
  proxy_username:
  proxy_password:
  use_proxy:
  url_username:
  url_password:
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }
```