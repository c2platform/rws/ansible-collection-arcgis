# Ansible Role c2platform.gis.vertigis_studio

This Ansible role can be used to install and manage VertiGIS Studio.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`vertigis_studio_cacerts2_certificates`](#vertigis_studio_cacerts2_certificates)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `vertigis_studio_cacerts2_certificates`

Use dict `vertigis_studio_cacerts2_certificates` to create / manage SSL/TLS
certificates. The example configuration belows shows creation and deployment of
a certificate `C:/ProgramData/Certs/geoweb-GSD-GEOWEB.p12`.

```yaml
vertigis_studio_cacerts2_certificates:
  - common_name: "{{ gs_geoweb_certificate_friendly_name }}"
    country_name: NL
    locality_name: Den Haag
    organization_name: C2
    organizational_unit_name: C2 GIS Platform Team
    state_or_province_name: Zuid-Holland
    subject_alt_name: "{{ gs_certificate_san }}"
    ansible_group: geoweb
    deploy:
      p12:
        # dest: C:/ProgramData/Certs/geoweb.pfx
        dir: C:/ProgramData/Certs
```

Refer to
[Setting Up Geoweb | C2 Platform](https://c2platform.org/docs/howto/rws/arcgis/geoweb/)
for more information.

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }
```