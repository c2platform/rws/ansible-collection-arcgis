# Ansible Role c2platform.gis.arcgis_webadaptor

This Ansible role can be used to install / manage
[ArcGIS Web Adaptor](https://enterprise.arcgis.com/en/server/latest/install/windows/about-the-arcgis-web-adaptor.htm) on MS
Windows. This basically automates installation as described in
[ArcGIS Web Adaptor installation guide](https://enterprise.arcgis.com/en/web-adaptor/latest/install/iis/welcome-arcgis-web-adaptor-install-guide.htm).

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`arcgis_webadaptor_iis_features`](#arcgis_webadaptor_iis_features)
  - [`arcgis_webadaptor_arguments`](#arcgis_webadaptor_arguments)
  - [`arcgis_webadaptor_certificate`](#arcgis_webadaptor_certificate)
  - [`arcgis_webadaptor_site`](#arcgis_webadaptor_site)
  - [`arcgis_webadaptor_version`](#arcgis_webadaptor_version)
  - [`arcgis_webadaptor_versions`](#arcgis_webadaptor_versions)
  - [`arcgis_webadaptor_win_resources` and `arcgis_webadaptor_win_resources_types`](#arcgis_webadaptor_win_resources-and-arcgis_webadaptor_win_resources_types)
  - [`arcgis_webadaptor_dsc_resources` (Experimental)](#arcgis_webadaptor_dsc_resources-experimental)
  - [`arcgis_webadaptor_configure`](#arcgis_webadaptor_configure)
- [Dependencies](#dependencies)
- [Example Playbook](#example-playbook)


## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `arcgis_webadaptor_iis_features`

The `arcgis_webadaptor_iis_features` list is a pivotal component used to specify
the Microsoft Windows features required for the installation of Internet
Information Services (IIS). This list is subsequently utilized by the
`ansible.windows.win_feature` module to orchestrate the installation process.

Here is an example of the `arcgis_webadaptor_iis_features` list:

```yaml
arcgis_webadaptor_iis_features:
  - Web-Server
  - Web-Common-Http
  - Web-Default-Doc
  - Web-Static-Content
  - Web-Security
  - Web-Filtering
  - Web-Windows-Auth
  - Web-ISAPI-Ext
  - Web-ISAPI-Filter
  - Web-WebSockets
  - Web-Mgmt-Console
```

### `arcgis_webadaptor_arguments`

The `arcgis_webadaptor_arguments` string serves as a vital configuration element used to define the parameters that are to be provided to `Setup.exe` during the installation of Web Adaptors.

Here's an example of the `arcgis_webadaptor_arguments` string:

```yaml
arcgis_webadaptor_arguments: >-
  ACCEPTEULA=yes
  WEBSITE_ID=1
  PORT=443
  CONFIGUREIIS=TRUE
```

### `arcgis_webadaptor_certificate`

The `arcgis_webadaptor_certificate` configuration is employed to generate a
TLS/SSL server certificate specifically designed for IIS (Internet Information
Services). This certificate creation is facilitated using the
`c2platform.wincore.win_certificate` Ansible module.

An illustrative example of `arcgis_webadaptor_certificate` configuration is as
follows:

```yaml
arcgis_webadaptor_certificate:
  dns_name: "{{ ansible_hostname }}"
  validity_days: 3650
  state: present
```

### `arcgis_webadaptor_site`

The `arcgis_webadaptor_site` dictionary is employed to configure an HTTPS
binding using the `community.windows.win_iis_webbinding` Ansible module. For
detailed information on the parameters that can be utilized with this
dictionary, please refer to the documentation of the `win_iis_webbinding`
module.

Here's an example usage of `arcgis_webadaptor_site`:


```yaml
arcgis_webadaptor_site:
  name: Default Web Site
  protocol: https
  port: 443
  ip: "*"
```

### `arcgis_webadaptor_version`

Utilize the `arcgis_webadaptor_version` variable to configure the specific version
of ArcGIS Web Adaptor for installation:

```yaml
arcgis_webadaptor_version: 11.1
```

### `arcgis_webadaptor_versions`

use the `arcgis_webadaptor_versions` dictionary to define the available ArcGIS
Web Adaptor versions that can be installed. This dictionary should include a key for
each version, such as `11.1`. Beneath each version key, you can specify
attributes.

This role will include the role
[`c2platform.wincore.download`](https://gitlab.com/c2platform/rws/ansible-collection-wincore)
to perform downloads so refer to the documents for attributes that can be used
to configure the way download will be performed, verified and cleaned up.

```yaml
arcgis_webadaptor_version: 11.1
arcgis_webadaptor_versions:
  11.1:
    webadaptor:
      url: http://rws-iavw-age208.ad.rws.nl/software/arcgis/11.1/WebAdaptorIIS.zip
    webdeploy:
      url: http://rws-iavw-age208.ad.rws.nl/software/microsoft/WebDeploy_amd64_en-US.msi
    dotnet:
      url: http://rws-iavw-age208.ad.rws.nl/software/microsoft/dotnet-hosting-6.0.16-win.exe
```

### `arcgis_webadaptor_win_resources` and `arcgis_webadaptor_win_resources_types`

The `arcgis_webadaptor_win_resources` variable and
`arcgis_webadaptor_win_resources_types` are designed to manage a wide range of
resources through various Ansible modules. For this purpose it integrates the
`c2platform.wincore.win` role. For comprehensive details and guidelines, you can
refer to the documentation
[documentation](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/tree/master/roles/win)
associated with this role.

This variable can for example be used to install **WebDeploy**, **.Net Hosting
Bundle** and **Web Adaptor** for `server` and `portal` with configuration shown
below:

```yaml
arcgis_webadaptor_win_resources:
  - name: Install WebDeploy and DotNet Hosting
    type: win_package
    defaults:
      state: present
    resources:
      - path: "{{ 'WebDeploy_amd64_en-US.msi' | c2platform.wincore.download_file_path(arcgis_webadaptor_temp_dir['path']) | c2platform.core.replace_slashes }}"
        arguments: ADDLOCAL=ALL LicenseAccepted="0"  # idempotent
      - path: "{{ 'dotnet-hosting-8.0.6-win.exe' | c2platform.wincore.download_file_path(arcgis_webadaptor_temp_dir['path']) }}"
        arguments: /quiet /install /norestart LicenseAccepted="0"
        product_id: '{B34F1F60-4DE1-3BFE-8C0D-445545833B11}'  # not idempotent without this line
  - name: Install Web Adaptors
    type: win_package
    defaults:
      state: present
      path: "{{ 'WebAdaptorIIS.zip' | c2platform.wincore.download_extract_folder(arcgis_webadaptor_temp_dir['path']) }}\\Setup.exe"
    resources:
      - arguments: "{{ arcgis_webadaptor_arguments }} VDIRNAME=server"
        creates_path: C:\inetpub\wwwroot\server
      - arguments: "{{ arcgis_webadaptor_arguments }} VDIRNAME=portal"
        creates_path: C:\inetpub\wwwroot\portal
```

### `arcgis_webadaptor_dsc_resources` (Experimental)

The `arcgis_webadaptor_dsc_resources` dictionary allows you to install **Web
Adaptors** using the `ArcGIS_Install` resource from the [ArcGIS PowerShell DSC
Module](https://github.com/Esri/arcgis-powershell-dsc).

This feature is designed for experimentation to evaluate how effectively this
PowerShell module can provision resources using Ansible in conjunction with the
`ansible.windows.win_dsc` module.

```yaml
arcgis_webadaptor_dsc_resources:
  - name: Install ArcGIS Web Adaptors
    type: ArcGIS_Install
    defaults:
      Name: WebAdaptorIIS
      Path: C:\vagrant\tmp\WebAdaptorIIS\Setup.exe
      Version: "{{ arcgis_webadaptor_version }}"
      Ensure: Present
    resources:
      - WebAdaptorContext: portal
      - WebAdaptorContext: server
```

To proceed, you will need the [ArcGIS PowerShell DSC
Module](https://github.com/Esri/arcgis-powershell-dsc), which can be installed
using `arcgis_webadaptor_win_resources` as shown below:

```yaml
arcgis_webadaptor_win_resources:
  - name: ArcGIS
    type: win_psmodule
    state: present
```

### `arcgis_webadaptor_configure`

The `arcgis_webadaptor_configure` list plays a pivotal role in the configuration
of Web Adaptors. It allows you to specify a set of parameters that will be used
when executing `ConfigureWebAdaptor.exe`.

Here's an illustrative example of the `arcgis_webadaptor_configure` list:

```yaml
arcgis_webadaptor_configure:
  - mode: server
    url_wa: https://gsd-agportal1.internal.c2platform.org/server/webadaptor
    url: https://gsd-agserver1.internal.c2platform.org:6443/
    username: siteadmin
    password: "{{ gs_arcgis_server_admin_password }}"  # vault
    admin-access: 'true'
  - mode: portal
    url_wa: https://gsd-agportal1.internal.c2platform.org/portal/webadaptor
    url: https://gsd-agportal1.internal.c2platform.org:7443
    username: portaladmin
    password: "{{ gs_arcgis_portal_admin_password }}"  # vault
    admin-access: 'true'
```

This list allows you to configure Web Adaptors for different modes, such as
server and portal, by providing essential parameters like URLs, usernames, and
passwords. These settings are instrumental in tailoring the Web Adaptor
configurations to meet specific deployment requirements.

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

```yaml
---
- name: ArcGIS Web Adaptor
  hosts: gs_portal

  roles:
    - { role: c2platform.wincore.win, tags: ["windows", "v0"]}
    - { role: c2platform.gis.arcgis_webadaptor, tags: ["arcgis", "web-adaptor", "v1"] }

arcgis_webadaptor_win_resources:
  - name: Install WebDeploy and DotNet Hosting
    type: win_package
    defaults:
      state: present
    resources:
      - path: "{{ 'WebDeploy_amd64_en-US.msi' | c2platform.wincore.download_file_path(arcgis_webadaptor_temp_dir['path']) | c2platform.core.replace_slashes }}"
        arguments: ADDLOCAL=ALL LicenseAccepted="0"  # idempotent
      - path: "{{ 'dotnet-hosting-8.0.6-win.exe' | c2platform.wincore.download_file_path(arcgis_webadaptor_temp_dir['path']) }}"
        arguments: /quiet /install /norestart LicenseAccepted="0"
        product_id: '{B34F1F60-4DE1-3BFE-8C0D-445545833B11}'  # not idempotent without this line
  - name: Install Web Adaptors
    type: win_package
    defaults:
      state: present
      path: "{{ 'WebAdaptorIIS.zip' | c2platform.wincore.download_extract_folder(arcgis_webadaptor_temp_dir['path']) }}\\Setup.exe"
    resources:
      - arguments: "{{ arcgis_webadaptor_arguments }} VDIRNAME=server"
        creates_path: C:\inetpub\wwwroot\server
      - arguments: "{{ arcgis_webadaptor_arguments }} VDIRNAME=portal"
        creates_path: C:\inetpub\wwwroot\portal
```

See
[Setup ArcGIS Portal and ArcGIS Web Adaptors using Ansible | C2Platform](https://c2platform.org/en/docs/howto/rws/arcgis/portal/)
or an example on how this Ansible role can be used.

