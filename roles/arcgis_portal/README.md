# Ansible Role c2platform.gis.arcgis_portal

This Ansible role can be used to install / manage ArcGIS Portal on MS Windows.
It can also webstyles and initialize / create a portal site.

<!--
TODO firewall rules are "changed" each run!?
TODO portal requires a arcgis server instance?
-->

- [Ansible Role c2platform.gis.arcgis\_portal](#ansible-role-c2platformgisarcgis_portal)
  - [Requirements](#requirements)
  - [Role Variables](#role-variables)
    - [`arcgis_portal_service_account`](#arcgis_portal_service_account)
    - [`arcgis_portal_admin_account`](#arcgis_portal_admin_account)
    - [`arcgis_portal_install`](#arcgis_portal_install)
    - [`arcgis_portal_temp_dir`](#arcgis_portal_temp_dir)
    - [`arcgis_portal_version`](#arcgis_portal_version)
    - [`arcgis_portal_versions`](#arcgis_portal_versions)
    - [`arcgis_portal_health_check`](#arcgis_portal_health_check)
    - [`arcgis_portal_status_check`](#arcgis_portal_status_check)
    - [`arcgis_portal_firewall_ports`](#arcgis_portal_firewall_ports)
  - [Dependencies](#dependencies)
  - [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

* ArcGIS Portal software and license.
* This role necessitates that installation files be obtained in ZIP format.
  Consequently, you must extract the original ArcGIS Portal executable file and
  subsequently repackage it into a ZIP archive.

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `arcgis_portal_service_account`

To configure the service account credentials that the ArcGIS Portal Windows
service will utilize, utilize the `arcgis_portal_service_account` dictionary.
These settings are crucial, as they will be employed by the `setup.exe`
installer to set the `USER_NAME` and `PASSWORD` parameters.

Here's a sample YAML configuration:

```yaml
arcgis_portal_service_account:
  name: com.example\svc-whatever
  password: supersecret  # vault
```

### `arcgis_portal_admin_account`

The `arcgis_portal_admin_account` dictionary is your tool to define the
parameters for creating the local admin account. This admin account is essential
for managing and maintaining ArcGIS Portal. You can specify the `name`,
`password`, and `email` for this account, and these values will used when the
`createportal.bat` is executed by Ansible in [tasks/portal](./tasks/portal.yml).

```yaml
arcgis_portal_admin_account:
  name: portaladmin
  password: supersecret  # vault
  email: admin@somedomain.com
```

For further customization and detailed arguments, refer to the
`arcgis_portal_create_arguments` section in the `defaults/main.yml` file.

### `arcgis_portal_install`

To tailor the installation and content directories for your deployment, employ
the `arcgis_portal_install` dictionary. These settings are critical parameters
passed to the `setup.exe` installer to define the `CONTENTDIR` and `INSTALLDIR`.

Here's a sample YAML configuration:

```yaml
arcgis_portal_install:
  dir: d:\apps\ArcGIS
  content-dir: d:\arcgis\arcgisportal
```

For further customization and detailed arguments, refer to the
`arcgis_portal_install_arguments` section in the `defaults/main.yml` file. This
provides additional flexibility in configuring your ArcGIS Portal installation
to align with your specific requirements.

### `arcgis_portal_temp_dir`

The `arcgis_portal_temp_dir` dictionary provides configuration options
for specifying the directory used for downloading the zip file containing
installation files. By default, the role will create the specified directory or
directories (recursively) if they do not exist.

Here are the available options:

1. `path`: Set the path to the desired directory.
2. `create`: Change to `no` if you prefer the role not to create the directory.
3. `delete`: Change to `yes` if you want the directory, including the zip file
   and extracted contents, to be deleted after a successful installation.
4. `delete_files`: Change to `no` if you do not want Ansible to remove the zip
   file and the folder containing extracted contents.

Example configuration:

```yaml
arcgis_portal_temp_dir:
  path: D:\temp
  create: yes
  delete: no
  recursive: yes
  delete_files: yes
```
### `arcgis_portal_version`

Utilize the `arcgis_portal_version` variable to configure the specific version
of ArcGIS Portal for installation:

```yaml
arcgis_portal_version: 11.1
```

### `arcgis_portal_versions`

use the `arcgis_portal_versions` dictionary to define the available ArcGIS
Portal versions that can be installed. This dictionary should include a key for
each version, such as `11.1`. Beneath each version key, you can specify
attributes.

This role will include the role
[`c2platform.wincore.download`](https://gitlab.com/c2platform/rws/ansible-collection-wincore)
to perform downloads so refer to the documents for attributes that can be used
to configure the way download will be performed, verified and cleaned up.

```yaml
arcgis_portal_versions:
  11.1:
    portal:
      url: http://example.com/software/arcgis/11.1/PortalForArcGIS.zip
      product_id: {BED48866-C615-4790-AD87-01F114C1A999}
      # checksum:
      # force: true  # enable to download file each ansible run
    webstyles:
      url: http://example.com/software/arcgis/11.1/ArcGISWebStyles.zip
      product_id: {67EDD399-CBD8-48C8-8B72-D79FBBBD79B2}
      enabled: yes
    license:
      url: http://example.com/software/arcgis/11.1/portal.json
```

### `arcgis_portal_health_check`

Dict `arcgis_portal_health_check` is used to check if ArcGIS Portal is ready and
operational using `ansible.windows.win_uri`. The check is default configured as
shown below:

```yaml
arcgis_portal_health_check:
  url: https://localhost:7443/arcgis/portaladmin/healthCheck
  delay: 10
  retries: 20
  search: Health Check successful, the portal is ready
```

The check can be disabled using attribute `enabled`.

```yaml
arcgis_portal_health_check:
  enabled: false
```

### `arcgis_portal_status_check`

The `arcgis_portal_status_check` dictionary is employed to configure
a status check for ArcGIS Portal. The default configuration for this check is as
follows:

```yaml
arcgis_portal_status_check:
  url: https://localhost:7443/arcgis/portaladmin/?f=json
  delay: 3
  retries: 3
  username: "{{ arcgis_portal_admin_account['name'] }}"
  password: "{{ arcgis_portal_admin_account['password'] }}"
```

### `arcgis_portal_firewall_ports`

The `arcgis_portal_firewall_ports` list contains vital information concerning
Microsoft Firewall settings and the associated ports necessary for the proper
functioning of ArcGIS Pro.

> Please note that this list is not used to directly configure ports within
> ArcGIS Portal. This role does not manage the ports utilized by ArcGIS Portal.
> Instead, it is specifically designed to facilitate the configuration of
> Microsoft Firewall settings to accommodate ArcGIS Pro's port requirements.

```yaml
arcgis_portal_firewall_ports:
  - name: Apache
    port: 7443
  - name: ArcGIS Ephemeral
    port: 1024-5000
  - name: ArcGIS Ephemeral
    port: 49152-65535
  - name: Highly available
    port: 5701-5703
  - name: Highly available
    port: 7120
  - name: ArcGIS Highly available
    port: 7654
```

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

See [Setup ArcGIS Portal and ArcGIS Web Adaptors using Ansible | C2Platform](https://c2platform.org/en/docs/howto/rws/arcgis/portal/) for an example
on how this Ansible role can be used.
