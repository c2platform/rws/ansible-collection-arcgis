# Ansible Role c2platform.gis.arcgis_server

This Ansible role can be used to install / License / create ArcGIS server site and manage ArcGIS Server on MS Windows.

<!--
TODO firewall rules are "changed" each run!?
-->

- [Ansible Role c2platform.gis.arcgis\_server](#ansible-role-c2platformgisarcgis_server)
  - [Requirements](#requirements)
  - [Role Variables](#role-variables)
    - [`arcgis_server_role`](#arcgis_server_role)
    - [`arcgis_server_service_account`](#arcgis_server_service_account)
    - [`arcgis_server_admin_account`](#arcgis_server_admin_account)
    - [`arcgis_server_install`](#arcgis_server_install)
    - [`arcgis_server_temp_dir`](#arcgis_server_temp_dir)
    - [`arcgis_server_create_arguments`](#arcgis_server_create_arguments)
    - [`arcgis_server_version`](#arcgis_server_version)
    - [`arcgis_server_versions`](#arcgis_server_versions)
      - [ArcGIS Server Roles:](#arcgis-server-roles)
      - [ArcGIS Server Licenses:](#arcgis-server-licenses)
    - [`arcgis_server_firewall_ports`](#arcgis_server_firewall_ports)
    - [`arcgis_server_for_arcgis_sleep_time`](#arcgis_server_for_arcgis_sleep_time)
    - [`arcgis_server_license_command`](#arcgis_server_license_command)
    - [`arcgis_server_setup_arguments`](#arcgis_server_setup_arguments)
  - [Dependencies](#dependencies)
  - [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

* ArcGIS server software and license.
* This role necessitates that installation files be obtained in ZIP format.
  Consequently, you must extract the original ArcGIS Server executable files and
  subsequently repackage them into a ZIP archives.

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `arcgis_server_role`
To assign a ArcGIS server role to a host you need to add the following to the inventory file(hosts.ini) `arcgis_server_role=hosting`.
The arcgis_server_role variable can set to one of the following values:

* hosting
* server
* image
* geoanalytics
* geoevent
* knowledge
* workflow


Here's a sample hosts.ini configuration:

```ini
[gs_server]
server.com.example arcgis_server_role=server
hosting.com.example arcgis_server_role=hosting
image.com.example arcgis_server_role=image
```
### `arcgis_server_service_account`

To configure the service account credentials that the ArcGIS Server Windows
service will utilize, utilize the `arcgis_server_service_account` dictionary.
These settings are crucial, as they will be employed by the `setup.exe`
installer to set the `USER_NAME` and `PASSWORD` parameters.

Here's a sample YAML configuration:

```yaml
arcgis_server_service_account:
  name: com.example\svc-whatever
  password: supersecret  # vault
```

### `arcgis_server_admin_account`

The `arcgis_server_admin_account` dictionary is your tool to define the
parameters for creating the siteadmin account. This admin account is essential
for managing and maintaining ArcGIS Server. You can specify the `name` and
`password` for this account, and these values will used when the
`createsite.bat` is executed by Ansible in [tasks/arcgis_server_site](./tasks/arcgis_server_site.yml).

```yaml
arcgis_server_admin_account:
  name: portaladmin
  password: supersecret  # vault
```

### `arcgis_server_install`

To tailor the installation for your deployment, employ
the `arcgis_server_install` dictionary. These settings are critical parameters
passed to the `setup.exe` installer and will also be used for licensing.


- `dir: D:\apps\ArcGIS` is used for the installation home.
- `base: D:\arcgis` is used for the configuration and directories location. This will also be used to set the file ACL for the ArcGIS server service account to have full access to all file in this folder.
- `site_config` is used the ArcGIS sever site configuration.
- `directories` is used for the ArcGIS server content.
- `sysgen_path` is used by Ansible to do a check if the ArcGIS server is licensed.

Here's a sample YAML configuration:

```yaml
arcgis_server_install:
  dir: D:\apps\ArcGIS
  base: D:\arcgis
  site_config: D:\\arcgis\\arcgisserver\\config-store
  directories: D:\\arcgis\\arcgisserver\\directories
  sysgen_path: "C:\\Program Files\\ESRI\\License{{ arcgis_server_version }}\\sysgen\\keycodes"

```

For further customization and detailed arguments, refer to the
`arcgis_server_install_arguments` section in the `defaults/main.yml` file. This
provides additional flexibility in configuring your ArcGIS Portal installation
to align with your specific requirements.  
You can find more arguments at [Installation command line parameters.](https://enterprise.arcgis.com/en/server/11.1/install/windows/silently-install-arcgis-server.htm#ESRI_SECTION1_053571A731B84759B8208D8C6748E51A)

### `arcgis_server_temp_dir`

The `arcgis_server_temp_dir` dictionary provides configuration options
for specifying the directory used for downloading the zip file containing
installation files. By default, the role will create the specified directory or
directories (recursively) if they do not exist.

Here are the available options:

1. `path`: Set the path to the desired directory.
2. `create`: Change to `no` if you prefer the role not to create the directory.
3. `delete`: Change to `true` if you want the directory, including the zip file
   and extracted contents, to be deleted after a successful installation.
4. `delete_files`: Change to `no` if you do not want Ansible to remove the zip
   file and the folder containing extracted contents.

Example configuration:

```yaml
arcgis_server_temp_dir:
  path: D:\temp
  create: yes
  delete: no
  recursive: true
  delete_files: true
```

### `arcgis_server_create_arguments`
For further customization and detailed arguments, refer to the
`arcgis_server_create_arguments` section in the `defaults/main.yml` file.  
You can also find more arguments at [Using the createsite command line utility](https://enterprise.arcgis.com/en/server/11.1/install/windows/silently-install-arcgis-server.htm#ESRI_SECTION1_EDF7ACDDAD2842B2BA61BEBF712D3EB8)


### `arcgis_server_version`

Utilize the `arcgis_server_version` variable to configure the specific version
of ArcGIS Server for installation:

```yaml
arcgis_server_version: "11.1"
```

### `arcgis_server_versions`

#### ArcGIS Server Roles:
Use the `arcgis_server_versions` dictionary to define the available ArcGIS
Server versions that can be installed. This dictionary should include a dictionary for
each version, such as `"11.1"`. Beneath each version dictionary you must include a list of
ArcGIS server roles. Each of ArcGIS server roles needs the following attributes
url:, product_id: and unzip:.

#### ArcGIS Server Licenses:
You will also have to create a list of roles for the license files locations under the `licenses:` dictionary
with the following attributes. Roles name `hosting:` with the attribute `url:`. The url parameter needs to point to the license file location for the hosting server.

This role will include the role
[`c2platform.wincore.download`](https://gitlab.com/c2platform/rws/ansible-collection-wincore)
to perform downloads so refer to the documents for attributes that can be used
to configure the way download will be performed, verified and cleaned up.

Example configuration:
```yaml
arcgis_server_versions:
  "11.1":
    server:
      url: http://example.com/software/arcgis/11.1/ArcGISServer.zip
      product_id: "{0F6C2D4F-9D41-4D25-A8AF-51E328D7CD8F}"
      unzip: true
    geoevent:
      url: http://example.com/software/arcgis/11.1/ArcGISGeoEventServer.zip
      product_id: "{475EA5B0-E454-4870-BB1F-AB81EDDEC2A7}"
      unzip: true
    workflow:
      url: http://example.com/software/arcgis/11.1/ArcGISWorkflowManagerServer.zip
      product_id: "{BCCADE20-4363-4D62-AE55-BB51329210CF}"
      unzip: true
    licenses:
      hosting:
        url: http://example.com/software/arcgis/11.1/hosting.ecp
      server:
        url:  http://example.com/software/arcgis/11.1/server.ecp
      image:
        url: http://example.com/software/arcgis/11.1/image.ecp
      geoanalytics:
        url: http://example.com/software/arcgis/11.1/geoanalytics.ecp
      geoevent:
        url: http://example.com/software/arcgis/11.1/geoevent.ecp
      knowledge:
        url: http://example.com/software/arcgis/11.1/knowledge.ecp
      workflow:
        url: http://example.com/software/arcgis/11.1/workflow.ecp

```


### `arcgis_server_firewall_ports`

The `arcgis_server_firewall_ports` list contains vital information concerning
Microsoft Firewall settings and the associated ports necessary for the proper
functioning of ArcGIS Server.

> Please note that this list is not used to directly configure ports within
> ArcGIS Server. This role does not manage the ports utilized by ArcGIS Server.
> Instead, it is specifically designed to facilitate the configuration of
> Microsoft Firewall settings to accommodate ArcGIS Servers port requirements.

```yaml
arcgis_server_firewall_ports:
  server:
    - name: ArcGIS server https port for Apache Tomcat
      port: 6443
    - name: ArcGIS Server for internal processes
      port: 6006
  geoevent:
    - name: ArcGIS GeoEvent Server https
      port: 6143
```

### `arcgis_server_for_arcgis_sleep_time`

This is used to set a wait timer. This is the time Ansible will wait before trying to check if ArcGIS server is up. This time is set in seconds.

### `arcgis_server_license_command`
This points to the ArcGIS licesne command. `"C:\Program Files\Common Files\ArcGIS\bin\SoftwareAuthorization.exe"`

### `arcgis_server_setup_arguments`
You can find more info on this at [Installation command line parameters.](https://enterprise.arcgis.com/en/server/11.1/install/windows/silently-install-arcgis-server.htm#ESRI_SECTION1_053571A731B84759B8208D8C6748E51A)

## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

See [Setup ArcGIS Portal and ArcGIS Web Adaptors using Ansible | C2Platform](https://c2platform.org/en/docs/howto/rws/arcgis/portal/) for an example
on how this Ansible role can be used.
