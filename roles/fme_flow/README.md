# Ansible Role c2platform.gis.fme_flow

This role facilitates the installation of
[FME Flow](https://docs.safe.com/fme/html/FME-Flow/Home.htm)
on MS Windows hosts by harnessing the installer's ability to perform a "silent"
installation. You can find detailed instructions on how to perform a silent
installation on Windows in the
[Performing a Silent Installation (Windows)](https://docs.safe.com/fme/html/FME-Flow/AdminGuide/Silent-Install-Windows.htm)
 guide.

- [Requirements](#requirements)
- [Role Variables](#role-variables)
  - [`fme_flow_version` and `fme_flow_versions`](#fme_flow_version-and-fme_flow_versions)
  - [`fme_flow_folders`](#fme_flow_folders)
  - [`fme_flow_debug`](#fme_flow_debug)
  - [`fme_flow_extract_command` and `fme_flow_extract_command_enabled`](#fme_flow_extract_command-and-fme_flow_extract_command_enabled)
  - [`fme_flow_install_command`](#fme_flow_install_command)
  - [`fme_flow_install_command_ignore_errors`](#fme_flow_install_command_ignore_errors)
  - [`fme_flow_services`](#fme_flow_services)
  - [`fme_flow_include_role_tomcat`](#fme_flow_include_role_tomcat)
  - [`fme_flow_win_resources` and `fme_flow_win_resources_types`](#fme_flow_win_resources-and-fme_flow_win_resources_types)
  - [`fme_flow_config`](#fme_flow_config)
  - [`fme_flow_cacerts2_certificates`](#fme_flow_cacerts2_certificates)
  - [`fme_flow_services` and `fme_flow_services_manage`](#fme_flow_services-and-fme_flow_services_manage)
  - [`fme_flow_database`](#fme_flow_database)
- [Handlers](#handlers)
  - [Restart FME Flow services](#restart-fme-flow-services)
  - [Wait for WebSocket / 7078](#wait-for-websocket--7078)
- [Dependencies](#dependencies)
- [Known Issues](#known-issues)
  - [Extract exit code](#extract-exit-code)
- [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

## Role Variables

<!--  A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (/group_vars/gs_pro/main.ymlie. hostvars, group vars, etc.) should be mentioned here as well. -->

### `fme_flow_version` and `fme_flow_versions`

The variables `fme_flow_version` and `fme_flow_versions` configure the download
settings using the Ansible role `c2platform.wincore.download`. The following
example demonstrates how to download and install FME, where the
`c2platform.wincore.download` role handles the download, and this FME Flow role
`c2platform.gis.fme_flow` proceeds with the installation of FME Flow.

```yaml
fme_flow_version: 20.23.1
fme_flow_versions:
  20.23.1:
    url: https://downloads.safe.com/fme/2023/win64/fme-flow-2023.1.1-b23631-win-x64.exe
    checksum: f20f095631e473a6abe8b26dee7fbf60bb66f21e437b57ac2b6f2fc8e4e04e95
    checksum_algorithm: sha256
```

You can configure multiple downloads using these variables. The following
example demonstrates how to configure the download of an additional Oracle
client alongside FME Flow.

```yaml
fme_flow_version: 20.23.1
fme_flow_versions:
  20.23.1:
    fme:
      url: "{{ gs_software_repository }}/fme/fme-flow-2023.1.1-b23631-win-x64.exe"
      checksum: f20f095631e473a6abe8b26dee7fbf60bb66f21e437b57ac2b6f2fc8e4e04e95
      checksum_algorithm: sha256
    oracle:
      url: "{{ gs_software_repository }}/oracle/instantclient-basic-windows.x64-23.4.0.24.05.zip"
      checksum: f02b42f83094321a0c9c8b226768438c46685ab78ff4aecdb3d2f69ec9fdc8bf
      checksum_algorithm: sha256
```

However, this configuration also requires changes to the
`fme_flow_installer_path` and `fme_flow_extraction_path` variables, which are
used in the role to execute the correct install command. These variables need to
use the extra key `fme` to retrieve the URL of the FME Flow installer:

```yaml
fme_flow_installer_path: >-
  {{ fme_flow_versions[fme_flow_version]["fme"]["url"]
  | c2platform.wincore.download_file_path(fme_flow_temp_dir["path"]) }}
fme_flow_extraction_path: >-
  {{ fme_flow_versions[fme_flow_version]["fme"]["url"]
  | c2platform.wincore.download_extract_folder(fme_flow_temp_dir["path"]) }}
```

For more information about the functionality of the download role, please see
the
[`c2platform.wincore.download`](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/tree/master/roles/download)
documentation.

### `fme_flow_folders`

Use list variable `fme_flow_folders` to create folders for FME Flow. Default
`fme_flow_home` and `fme_flow_logs` will be created.

### `fme_flow_debug`

Utilize the boolean variable `fme_flow_debug` to generate a file named
`install_extract_command.txt` within the `fme_flow_logs` directory. This file
will encapsulate both the extract and install commands.

> **Caution**: Given that this file includes sensitive information such as
> passwords, exercise caution by resetting `fme_flow_debug` to false. This ensures
> the removal of the file for security purposes.

### `fme_flow_extract_command` and `fme_flow_extract_command_enabled`

The `fme_flow_extract_command` variable empowers you to tailor the command for
extracting the installer. This role automatically performs extraction before
installation. However, if you prefer to streamline the process into a single
step, you can disable this behavior by toggling the
`fme_flow_extract_command_enabled` variable.

Here's an example configuration for fme_flow_extract_command in YAML format:

```yaml
fme_flow_extract_command: >-
 {{ fme_flow_installer_path }}
 -d"{{ fme_flow_extraction_path }}"
 -s -sp"EXTRACTONLY"
```

Customize the `fme_flow_extract_command` to suit your extraction needs, and
utilize `fme_flow_extract_command_enabled` to control the extraction and
installation behavior of this role.


### `fme_flow_install_command`

You have the flexibility to configure the installation command by customizing the string variable `fme_flow_install_command`. As an example, the YAML configuration for a default installation:

```yaml
fme_flow_extract_command: >-
 {{ fme_flow_installer_path }}
 -d"{{ fme_flow_extraction_path }}"
 -s -sp"EXTRACTONLY /qb /norestart"
```

> If the package is an MSI do not supply the `/qn`, `/log` or `/norestart`
> arguments.
> [ansible.windows.win_package](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_package_module.html)

You also have the option to skip the installation command when you want to
perform a manual installation by using `fme_flow_install_command_enabled`.

### `fme_flow_install_command_ignore_errors`

Use boolean `fme_flow_install_command_ignore_errors` ( default `false` ) to
ignore any errors produced when running the `fme_flow_install_command`. The
output of installer command will also be written to a file configured using
`fme_flow_install_command_result_yml`.

### `fme_flow_services`

Use this variable to manage FME Windows services using Ansible module
`ansible.windows.win_service`.

### `fme_flow_include_role_tomcat`

The `fme_flow_include_role_tomcat` dictionary allows you to install and manage
FME Flow with your own version of Tomcat by including a dedicated Ansible role.
This functionality is particularly useful when you need customized control over
the Tomcat environment as part of your FME Flow setup.

To utilize this feature, configure the dictionary as shown in the example below:

```yaml
fme_flow_include_role_tomcat:
  role: c2platform.gis.tomcat
  include: true
  service: "{{ tomcat_service['name'] }}"
```

- **Role Inclusion**: By setting `include: true`, the Ansible role
  `c2platform.gis.tomcat` is included within your current playbook using the
  `include_role` module. This inclusion ensures that the specified Tomcat role
  is executed as part of the FME Flow installation and configuration process.
  Adjust the `role` value as necessary to match your desired Tomcat
  configuration role.

- **Service Management**: Use the `service` variable to specify the name
  of the Tomcat service that should be restarted. This is typically required
  after installing or configuring FME Flow to ensure that all changes are
  applied successfully and the service runs with the updated settings. In the
  example, `service` is dynamically set using `{{ tomcat_service['name']
  }}`, so ensure this variable is correctly defined in your inventory or
  playbook.


When integrating Tomcat as part of your FME Flow deployment, it is recommended
to utilize the [`fme_flow_services`](#fme_flow_services) variable to specify the
Tomcat service as one of the managed services. This setup allows the role to
manage the service efficiently using the `ansible.windows.win_service` module.
In addition, the `fme_flow_services` variable is referenced by the Ansible
handler [Restart FME Flow services](#restart-fme-flow-services) to ensure
services are restarted appropriately.

Example configuration:

```yaml
fme_flow_services:
  - name: FME Server Core
    state: started
  - name: "{{ tomcat_service['name'] }}"
    state: started
```

By appropriately configuring `fme_flow_include_role_tomcat`, you can integrate
Tomcat management seamlessly into your Ansible workflows for FME Flow
deployments. This approach enhances the flexibility and control over your
infrastructure, allowing you to tailor deployments to fit the specific
requirements of your environment. Adjust the settings based on your
infrastructure needs and deployment strategies.

### `fme_flow_win_resources` and `fme_flow_win_resources_types`

The `fme_flow_win_resources` variable and `fme_flow_win_resources_types` are
designed to manage a wide range of resources through various Ansible modules.
For this purpose it integrates the
`c2platform.wincore.win` role. For comprehensive details and guidelines, you can
refer to the documentation
[documentation](https://gitlab.com/c2platform/rws/ansible-collection-wincore/-/tree/master/roles/win)
associated with this role.

When applied to this FME Flow role, `fme_flow_win_resources` can for example be
used to change `FME_SERVER_WEB_URL` in `fmeServerConfig.txt`. For a deeper
understanding and practical insights, the community website offers this guide:

* [Setting Up FME Flow with Ansible](https://c2platform.org/en/docs/howto/rws/arcgis/fme_flow/)

### `fme_flow_config`

In Fme Flow much can be configured by API's. To use these API's we first create a token. Next this token is used in an API to create a connection with an LDAP server. To make this API 'omnipotent' we first try to update any existing connections. This can give us either a 204 (updated LDAP server) or 404 (cant find any LDAP servers with specific name). If another statuscode is given, Ansible fails the task. As soon as an 404 code is given, we try to create a new LDAP connection. Again, if this fails, Ansible will fail this task. As an example, the YAML configuration for a default installation:

```yaml
- name: If LDAP connection to FME exists, then update
  ansible.windows.win_uri:
    url: "https://localhost/fmerest/v3/security/ldap/servers/AD.RWS.NL"
    method: PUT
    validate_certs: false
    body: authentication=Basic&enableSynchronization=true&encryption=StartTLS&host=ldap01.mydomain.com&port=389&redundantServers=ldap02.mydomain.com%3A389&searchUser={{ fme_flow_svcaccount }}&searchUserPassword={{ fme_flow_svcpassword }}&synchronizationInterval=3600
    return_content: yes
    headers:
      Content-Type: application/x-www-form-urlencoded
      Accept: application/json
      Authorization: FMEToken token={{ fme_flow_token }}
      status_code: [204, 404]  # 204:Succes, LDAP server replaced! 404:The LDAP server does not exist.
  register: fme_flow_uri_return
```

You also have the option to skip the config commands when you want to
perform a manual configuration by using `fme_flow_config_command_enabled`.

### `fme_flow_cacerts2_certificates`

Use dict `fme_flow_cacerts2_certificates` to create / manage SSL/TLS
certificates and / or Java KeyStores. The example configuration belows shows
creation and deployment of a private key, a certificate and a Java KeyStore to
the Tomcat `conf` directory.

```yaml
fme_flow_cacerts2_certificates:
  - common_name: "{{ inventory_hostname }}"  # alias
    country_name: NL
    locality_name: Den Haag
    organization_name: C2
    organizational_unit_name: C2 GIS Platform Team
    state_or_province_name: Zuid-Holland
    subject_alt_name:
      - "DNS:{{ inventory_hostname }}"
      - "DNS:{{ inventory_hostname }}.apps.c2platform.org"
    ansible_group: tomcat
    deploy:
      key:
        dir: "{{ tomcat_home }}/conf"
      crt:
        dir: "{{ tomcat_home }}/conf"
      keystore:
        dir: "{{ tomcat_home }}/conf"
        password: secret
```

Refer to
[Tomcat SSL/TLS and Java Keystore and TrustStore Configuration for Linux and Windows Hosts | C2 Platform](https://c2platform.org/en/docs/howto/rws/certs/)
for more information.

### `fme_flow_services` and `fme_flow_services_manage`

To manage the FME services created by the installer, you can utilize the
`fme_flow_services` variable. You can use all parameters of the Ansible module
[`ansible.windows.win_service`](https://docs.ansible.com/ansible/latest/collections/ansible/windows/win_service_module.html).
Here's an example configuration in YAML:

```yaml
fme_flow_services:
  core:
    name: FME Server Core
    state: started
fme_flow_services_manage: false
```

If you wish to refrain from using Ansible to manage the service, you can set the
`fme_flow_services_manage` variable to false. Keep in mind that this service will only
be managed when installation is enabled, which is controlled by the
`fme_flow_install_command_enabled` parameter.

### `fme_flow_database`

The `fme_flow_database` dictionary is used to configure the creation of a PostgreSQL
database, user, and database objects. By default, this feature is disabled,
which can be controlled using the `manage` attribute. However, please note that
there are certain limitations associated with this feature:

1. **Supported Database:** As of now, only PostgreSQL databases are supported.
2. **Task Delegation:** The tasks required for creating the database, user, and
   executing the schema script are delegated to run on the database server,
   utilizing the `postgres` user.

Below is the default configuration:

```yaml
fme_flow_database:
  manage: false
  type: postgresql
  inventory_hostname: gsd-db1
  user: fmeserver
  database: fmeserver
  query: >-
    SELECT * FROM information_schema.tables
    where table_name = 'fme_action';
  script:
    src: '{{ fme_flow_home }}\Server\database\postgresql\postgresql_createSchema.sql'
    dest: /tmp/postgresql_createSchema.sql
```

In this example, the `fme_flow_database` configuration is tailored to manage a
PostgreSQL database on the host labeled as `gsd`-db1. It specifies the user as
`fmeserver`, the database name as `fmeserver`, and includes a SQL query to
verify the existence of database objects. Furthermore, it defines a script that
initializes the schema. This script is retrieved from the FME node and copied to
the database host identified as `gsd-db1`.

Normally, most of the default settings do not require alteration. Typically, you
would only need to configure the `inventory_hostname` and set the `manage`
attribute to `true`:

```yaml
fme_flow_database:
  manage: true
  inventory_hostname: gsd-db1
```

## Handlers

### Restart FME Flow services

### Wait for WebSocket / 7078

This Ansible role contains a handler that waits for the WebSocket port (7078) to
open, utilizing the `ansible.windows.win_wait_for` module.

To notify the handler to wait for the WebSocket port to open, use the following
`notify` directive in your tasks:

```yaml
notify: Wait for WebSocket on Port 7078
```

The handler is configurable using the dictionary
`fme_flow_websocket_wait_handler`, which supports the following attributes:

| Attribute | Default | Description                                                            |
|-----------|---------|------------------------------------------------------------------------|
| `delay`   | `10`    | The number of seconds to wait before starting to check the port status |
| `timeout` | `60`    | The total amount of time (in seconds) to wait for the port to open     |
| `host`    | (unset) | The hostname or IP address to check the port on                        |
| `port`    | `7078`  | The port number to wait for                                            |
| `when`    | (unset) | Conditions under which the handler should be triggered                 |


## Dependencies

For FME to have a working webconsole you need to at least install Tomcat (and Java as a prerequisite for Tomcat).

- role: c2platform.gis.java
- role: c2platform.gis.tomcat

## Known Issues

### Extract exit code

When executing the extraction command from Ansible using
`ansible.windows.win_command`, it's worth noting that the exit code for
successful execution is not the typical `0` exit code when run from Ansible. It
also does not return a fixed code for "success".

```bash
C:\vagrant\tmp\fme-flow-2023.1.1-b23631-win-x64.exe -d"C:\vagrant\tmp\fme-flow-2023.1.1-b23631-win-x64" -s -sp"EXTRACTONLY /qb /norestart"
```

This discrepancy in exit codes has led to the implementation of a `failed_when: false`
condition in the Ansible task responsible for the extraction process.

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

1. First define which servers are going to have the core services installed and which servers are going to be the engines.

2. Run fme.yml (see example of this playbook below):

```yaml
---
- name: FME
  hosts: fme

  roles:
    # Prerequisites
    - {role: c2platform.wincore.win, tags: ["fme", "windows"]}
    # Randsoftware
    - {role: c2platform.gis.oracle, tags: ["fme", "oracle"]}
    - {role: c2platform.gis.java, tags: ["fme", "java"], when: 'inventory_hostname in groups["fme_core"]'}
    - {role: c2platform.gis.tomcat, tags: ["fme", "tomcat"], when: 'inventory_hostname in groups["fme_core"]'}
    # FME_Form
    - {role: c2platform.gis.fme_form, tags: ["fme", "form"], when: 'inventory_hostname in groups["fme_form"]'}
    # FME_Flow
    - {role: c2platform.gis.fme_core, tags: ["fme", "fme_core"], when: 'inventory_hostname in groups["fme_core"]'}
    - {role: c2platform.gis.fme_engine, tags: ["fme", "fme_engine"], when: 'inventory_hostname in groups["fme_engine"]'}
```

3. This wil run the fme role to install FME server. FME will be downloaded either via www or via a share, depending on the     default\main.yml

4. After the installation you will need to run the DataBase scripts (created by the FME installer) on your specific database

See also [ansible-gis/plays/gis/fme.yml](https://gitlab.com/c2platform/rws/ansible-gis/-/blob/320700423b214c65658e0b5faed0484f66bc86e4/plays/gis/fme.yml)


FME Flow Architecture
https://docs.safe.com/fme/2023.0/html/FME-Flow/ReferenceManual/architecture.htm