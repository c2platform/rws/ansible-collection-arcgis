# Ansible Role c2platform.gis.arcgis_datastore

This Ansible role can be used to manage [ArcGIS Data
Store](https://enterprise.arcgis.com/en/data-store/latest/install/windows/what-is-arcgis-data-store.htm).
See also [Install ArcGIS Data Store—ArcGIS
Enterprise](https://enterprise.arcgis.com/en/data-store/latest/install/windows/install-data-store.htm).

- [Ansible Role c2platform.gis.arcgis\_datastore](#ansible-role-c2platformgisarcgis_datastore)
  - [Requirements](#requirements)
  - [Role Variables](#role-variables)
    - [`arcgis_datastore_role`](#arcgis_datastore_role)
    - [`arcgis_datastore_version`](#arcgis_datastore_version)
    - [`arcgis_datastore_versions`](#arcgis_datastore_versions)
    - [`arcgis_datastore_temp_dir`](#arcgis_datastore_temp_dir)
    - [`arcgis_datastore_service_account`](#arcgis_datastore_service_account)
    - [`arcgis_datastore_install`](#arcgis_datastore_install)
    - [`arcgis_datastore_firewall`](#arcgis_datastore_firewall)
- [ArcGIS Data Store download URL](#arcgis-data-store-download-url)
  - [Dependencies](#dependencies)
  - [Example Playbook](#example-playbook)

## Requirements

<!-- Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required. -->

* ArcGIS Data Store software and license.
* ArcGIS Server instance. Default it is assumed you will register one or more
  data stores and for this you will need this instance. You can use
  `arcgis_datastore_ags_user`, `arcgis_datastore_ags_password` and
  `arcgis_datastore_ags_url` to configure the ArcGIS Server to use.

## Role Variables

### `arcgis_datastore_role`

The string `arcgis_datastore_role` can be used to configure the data stores to create. It defaults to `relational,tilecache`.

```yaml
arcgis_datastore_role: relational,tilecache
```

Other options are of course: `spatiotemporal`, `object`, `graph`.

If you want to configure all data stores on a server, you can use `arcgis_datastore_role_types`:

```
arcgis_datastore_role: "{{ arcgis_datastore_role_types | join(',') }}"
```
### `arcgis_datastore_version`
Utilize the `arcgis_datastore_version` variable to configure the specific version
of ArcGIS Data Store for installation:

```yaml
arcgis_datastore_version: "11.1"
```

### `arcgis_datastore_versions`
use the `arcgis_datastore_versions` dictionary to define the available ArcGIS
Data Store versions that can be installed. This dictionary should include a key for
each version, such as `11.1`. Beneath each version key, you can specify
attributes.

This role will include the role
[`c2platform.wincore.download`](https://gitlab.com/c2platform/rws/ansible-collection-wincore)
to perform downloads so refer to the documents for attributes that can be used
to configure the way download will be performed, verified and cleaned up.

```yaml
arcgis_datastore_versions:
  11.1:
    datastore:
      url: http://example.com/software/arcgis/11.1/ArcGISDataStore.zip
      product_id: "{391B3A39-0951-43E3-991D-82C82CA6E4A4}"
      unzip: true
    license:
      url: http://example.com/software/arcgis/11.1/portal.json
```
### `arcgis_datastore_temp_dir`

The `arcgis_datastore_temp_dir` dictionary provides configuration options
for specifying the directory used for downloading the zip file containing
installation files. By default, the role will create the specified directory or
directories (recursively) if they do not exist.

Here are the available options:

1. `path`: Set the path to the desired directory.
2. `create`: Change to `no` if you prefer the role not to create the directory.
3. `delete`: Change to `true` if you want the directory, including the zip file
   and extracted contents, to be deleted after a successful installation.
4. `delete_files`: Change to `no` if you do not want Ansible to remove the zip
   file and the folder containing extracted contents.

Example configuration:

```yaml
arcgis_datastore_temp_dir:
  path: D:\temp
  create: yes
  delete: no
  recursive: true
  delete_files: true
```

### `arcgis_datastore_service_account`
To configure the service account credentials that the ArcGIS Data Store Windows
service will utilize, utilize the `arcgis_datastore_service_account` dictionary.
These settings are crucial, as they will be employed by the `setup.exe`
installer to set the `USER_NAME` and `PASSWORD` parameters.

Here's a sample YAML configuration:

```yaml
arcgis_server_service_account:
  name: com.example\svc-whatever
  password: supersecret  # vault
```

### `arcgis_datastore_install`
To tailor the installation for your deployment, employ
the `arcgis_datastore_install` dictionary. These settings are critical parameters
passed to the `setup.exe` installer.


- `dir: D:\apps\ArcGIS` is used for the installation home.
- `base: D:\arcgis` is used for the configuration and directories location. This will also be used to set the file ACL for the ArcGIS Data Store service account to have full access to all file in this folder.
- `content_directory:` This is where ArcGIS Data Store will keep its user data.
- `sysgen_path` This is set but not used.

Here's a sample YAML configuration:

```yaml
arcgis_datastore_install:
  dir: D:\apps\ArcGIS
  base: D:\arcgis
  content_directory: D:\\arcgis\\arcgisdatastore
  sysgen_path: "C:\\Program Files\\ESRI\\License{{ arcgis_datastore_version }}\\sysgen\\keycodes"

```

<!--
`arcgis_datastore_install_temp_base:` This is where the install files will be downloaded to and then be extracted.
`arcgis_datastore_setup_file:` ArcGISDataStore   # The file name to download with out the extention
`arcgis_datastore_install_directory:` d:\apps\ArcGIS
`arcgis_datastore_content_directory:` d:\arcgis\arcgisdatastore
`arcgis_datastore_product_id:` {391B3A39-0951-43E3-991D-82C82CA6E4A4}
`arcgis_datastore_service_account:` Your Service account user.
`arcgis_datastore_service_account_password:` Your service account password.
`arcgis_datastore_ags_user:` ArcGIS server site admin account user name.
`arcgis_datastore_ags_password:` ArcGIS server site admin account password.
`arcgis_datastore_sleep_time:` 1
-->


### `arcgis_datastore_firewall`

The var `arcgis_datastore_firewall` is a dict with settings to configure
firewall using `community.windows.win_firewall_rule`.If you don't want to
configure the firewall you can use `arcgis_datastore_firewall_enabled` to
disable firewall configuration.

# ArcGIS Data Store download URL
arcgis_datastore_download_url: https://WebServerFQDN/.../.../.../ArcGISServer.zip"


## Dependencies

<!--   A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles. -->

## Example Playbook

<!--   Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too: -->

See [hosts.ini](https://gitlab.com/c2platform/rws/ansible-gis/-/blob/master/hosts.ini?ref_type=heads)

```yaml
    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }
```