# Ansible Collection - c2platform.arcgis ( DEPRECATED )

[![Pipeline Status](https://gitlab.com/c2platform/rws/ansible-collection-gis/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/pipelines) [![Latest Release](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/pipelines)

See full [README](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/blob/master/README.md).
