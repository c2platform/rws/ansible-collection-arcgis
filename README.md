# Ansible Collection - c2platform.gis ( DEPRECATED )

[![Pipeline Status](https://gitlab.com/c2platform/rws/ansible-collection-gis/badges/master/pipeline.svg?style=flat-square&key_text=Pipeline+Status&key_width=90)](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/pipelines)
[![Latest Release](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/badges/release.svg?style=flat-square)](https://gitlab.com/c2platform/rws/ansible-collection-gis/-/pipelines)
[![Ansible Galaxy](https://img.shields.io/badge/Galaxy-_c2platform.gis-blue.svg)](https://galaxy.ansible.com/ui/repo/published/c2platform/gis/)

C2 Platform Ansible Collection for ArcGIS Server, Pro, Portal, FME, etc

## Roles

### ArcGIS Enterprise

* [`arcgis_server`](./roles/arcgis_server)
* [`arcgis_datastore`](./roles/arcgis_datastore)
* [`arcgis_portal`](./roles/arcgis_portal)
* [`arcgis_webadaptor`](./roles/arcgis_webadaptor)
* [`arcgis_federation`](./roles/arcgis_federation)
* [`arcgis_pro`](./roles/arcgis_pro)

### Checkmk

* [`bridge`](./roles/bridge)
* [`cmk_agent`](./roles/cmk_agent)
* [`cmk_server`](./roles/cmk_server)

### FME

* [`fme_flow`](./roles/fme_flow)
* [`tomcat`](./roles/tomcat)
* [`java`](./roles/java)

### Other

* [`vertigis_studio`](./roles/vertigis_studio)

## Modules and Filters

For detailed information on the available modules and filters within this
collection, please refer to the
[Ansible Galaxy](https://galaxy.ansible.com/ui/repo/published/c2platform/gis/docs/)
page.

You can of course also use the `ansible-doc` command to explore the
documentation:

```bash
ansible-doc -t module --list c2platform.gis
ansible-doc -t filter --list c2platform.gis
ansible-doc -t filter <FILTER_NAME>
ansible-doc -t module <MODULE_NAME>
```
